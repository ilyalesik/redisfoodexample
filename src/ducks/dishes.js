import { takeEvery, put } from "redux-saga/effects";
import {DISHES} from "../fixtures/dishes";
import _ from 'lodash'
import { createSelector } from "reselect";

// Actions
const DISHES_SUCCESS = "dishes/DISHES_SUCCESS";
const DISHES_REQUEST = "dishes/DISHES_REQUEST";
const SELECT_DISH = "dishes/SELECT_DISH";

// Reducer
function getDefaultState() {
    return {
        dishes: [],
        selectedDishes: []
    };
}

export default function(state = getDefaultState(), action = {}) {
    switch (action.type) {
        case DISHES_SUCCESS:
            return { ...state, dishes: action.value };
        case SELECT_DISH:
            return {
                ...state,
                selectedDishes: state.selectedDishes.indexOf(action.id) < 0 ?
                    [...state.selectedDishes, action.id] : _.filter(state.selectedDishes, (id) => id !== action.id)
            }
    }
    return state;
}

// Action Creators
export function loadDishes() {
    return { type: DISHES_REQUEST };
}

export function loadDishesSuccess(value) {
    return { type: DISHES_SUCCESS, value };
}

export function selectDish(id) {
    return { type: SELECT_DISH, id}
}

//Selectors
const getSelectedDishes = state => state.dishes.selectedDishes;
const getDishes = state => state.dishes.dishes;

export const selectedDishesCount = createSelector([
    getSelectedDishes
], (selected) => {
    return selected.length;
});

export const getOrderDishes = createSelector([
    getSelectedDishes, getDishes
], (selected, dishes) => {
    return _.filter(dishes, (dish) => selected.indexOf(dish.id) >= 0);
});

export const getOrderTotal = createSelector([
    getOrderDishes
], (dishes) => {
    return _.reduce(dishes, (sum, item) => sum + (item.price || 0), 0);
});

// Sagas
export function* loadDishesSaga() {
    yield takeEvery(DISHES_REQUEST, loadDishesTask);
}

export function* loadDishesTask() {
    yield put(loadDishesSuccess(DISHES))
}