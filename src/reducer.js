import { combineReducers } from "redux";
import dishes from "./ducks/dishes"

export default combineReducers({
    dishes
});
