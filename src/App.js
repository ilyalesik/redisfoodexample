import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';
import {NativeRouter, Route} from "react-router-native";
import HomePage from "./components/HomePage/HomePage";
import Header from "./components/Header/Header";

import { Provider } from "react-redux";
import reducer from "./reducer";
import { composeWithDevTools } from "redux-devtools-extension";
import { createStore, applyMiddleware } from "redux";
import createSagaMiddleware from "redux-saga";
import {loadDishes, loadDishesSaga} from "./ducks/dishes";
import OrderPage from "./components/OrderPage/OrderPage";

const sagaMiddleware = createSagaMiddleware();

const store = createStore(
    reducer,
    {},
    composeWithDevTools(applyMiddleware(sagaMiddleware))
);

sagaMiddleware.run(loadDishesSaga);

export default class App extends Component {
  render() {
    return (
        <Provider store={store}>
            <NativeRouter>
                <View style={styles.container}>
                    <Header />
                    <Route exact path="/" component={HomePage}/>
                    <Route path="/order" component={OrderPage}/>
                </View>

            </NativeRouter>
        </Provider>
    );
  }
}



const styles = StyleSheet.create({
    container: {
        marginTop: 25,
    }
});
