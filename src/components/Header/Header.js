import React, { Component } from 'react';
import styled from 'styled-components';
import {connect} from "react-redux";
import {selectDish, selectedDishesCount} from "../../ducks/dishes";
import {Link} from "react-router-native";

const Container = styled.View`
      position: absolute;
      top: 0;
      left: 0;
      right: 0;
      backgroundColor: #FFFFFF;
      overflow: hidden;
      
      height: 50px;
      width: 100%;
      z-index: 50;
      
      padding: 10px;
      display: flex;
      flex-direction: row;
      align-items: center;
      justify-content: space-between;
`;

const Title = styled.Text`
    font-size: 18px;
`;

const MenuIcon = styled.Image`
    width: 24px;
    height: 24px;
`;

const Badge = styled.View`
    width: 24px;
    height: 24px;
    border-radius: 12px;
    border: solid 1px #979797;
    background-color: #f3f3f3;
    
    display: flex;
    align-items: center;
    justify-content: center;
`;

const BadgeText = styled.Text`
    color: #d0021b;
    font-size: 10;
`;

class Header extends Component{
    render() {
        return <Container>
            <MenuIcon source={require('./menu.png')} />
            <Link to='/'>
                <Title>Main Plates</Title>
            </Link>
            <Link to='/order'>
                <Badge>
                    <BadgeText>{"" + this.props.selectedCount}</BadgeText>
                </Badge>
            </Link>
        </Container>
    }
}

function mapStateToProps(state, ownProps) {
    return {
        selectedCount: selectedDishesCount(state)
    };
}

export default connect(mapStateToProps, {selectDish})(Header);