import React, { Component } from 'react';
import {ScrollView, Text, View} from "react-native";
import styled from 'styled-components'
import {getOrderDishes, getOrderTotal} from "../../ducks/dishes";
import {connect} from "react-redux";

const Container = styled.View`
    border-style: solid;
    border-top-width: 1px;
    border-top-color: rgba(151,151,151,0.51);
    padding-left: 10px;
    padding-right: 10px;
    
    margin-top: 50px;
`;

const Item = styled.View`
    border-style: solid;
    border-bottom-width: 1px;
    border-bottom-color: rgba(151,151,151,0.5);
    
    height: ${({total}) => total ? "60px" : "50px"};
    display: flex;
    flex-direction: row;
    align-items: center;
`;

const DishTitle = styled.Text`
    font-size: 16px;
    flex: 1;
`;

const DishPrice = styled.Text`
    font-size: 14px;
    color: #4a4a4a;
    opacity: 0.5;
`;

class OrderList extends Component {
    render() {
        return <Container>
            <ScrollView>
                {this.props.dishes.map((dish) => (
                    <Item key={dish.id}>
                        <DishTitle>{dish.title}</DishTitle>
                        {dish.price && <DishPrice>${dish.price}</DishPrice>}
                    </Item>
                ))}
                <Item total>
                    <DishTitle>TOTAL</DishTitle>
                    <DishPrice>${this.props.total}</DishPrice>
                </Item>
            </ScrollView>
        </Container>
    }
}

function mapStateToProps(state) {
    return {
        dishes: getOrderDishes(state),
        total: getOrderTotal(state)
    }
}

export default connect(mapStateToProps)(OrderList);