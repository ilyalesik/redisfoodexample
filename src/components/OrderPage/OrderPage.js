import React, { Component } from 'react';
import {ScrollView, Text, View} from "react-native";
import styled from 'styled-components'
import OrderList from "../OrderList/OrderList";

const Container = styled.View`
    margin-top: 50px;
`;

const BackgroundContainer = styled.View`
    width: 100%;
    height: 200px;
    
    display: flex;
    align-items: center;
    justify-content: flex-end;
`;

const Background = styled.Image`
    position: absolute;
    width: 100%;
    height: 100%;
    z-index: -5;
`;

const BackgroundMask = styled.View`
    position: absolute;
    width: 100%;
    height: 100%;
    z-index: -4;
    background: #000000;
    opacity: 0.2;
`;

const DateTitle = styled.Text`
    color: #FFFFFF;
    font-size: 20px;
    
    margin-bottom: 20px;
`;

export default class OrderPage extends Component {
    render() {
        return <Container>
            <BackgroundContainer>
                <Background source={{uri: "https://shkolazhizni.ru/img/content/i133/133774_or.jpg"}} />
                <BackgroundMask />
                <DateTitle>October 2014</DateTitle>
            </BackgroundContainer>


            <OrderList />
        </Container>
    }
}