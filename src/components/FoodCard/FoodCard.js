import React, { Component } from 'react';
import styled from "styled-components";
import PropTypes from 'prop-types'
import {Image, Text} from "react-native";
import FoodCardCheckbox from "./FoodCardCheckbox";
import FoodPrice from "./FoodPrice";
import {connect} from "react-redux";
import {selectDish} from "../../ducks/dishes";

const Container = styled.View`
    width: 100%;
    height: 300px;
    
    display: flex;
    justify-content: flex-end;
`;

const InnerContainer = styled.View`
    width: 100%;
    padding: 10px;
    
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
`;

const FoodCardImage = styled.Image`
    position: absolute;
    width: 100%;
    height: 100%;
    z-index: -5;
`;

const BackgroundMask = styled.View`
    position: absolute;
    width: 100%;
    height: 100%;
    z-index: -5;
    background: #000000;
    opacity: 0.4;
`;

const Title = styled.Text`
    color: #FFFFFF;
    font-size: 18px;
    font-weight: 700;
`;

const LeftList = styled.View`
    display: flex;
    align-items: flex-start;
`;

class FoodCard extends Component {

    constructor() {
        super();
    }

    static propTypes = {
        img: PropTypes.string,
        title: PropTypes.string,
        price: PropTypes.string
    };

    render() {
        const {img, title, price} = this.props;
        return <Container>
            <FoodCardImage source={{uri: img}} />
            <BackgroundMask />
            <InnerContainer>
                <LeftList>
                    {price && <FoodPrice price={price}/>}
                    <Title>{title}</Title>
                </LeftList>
                <FoodCardCheckbox checked={this.props.checked}
                                  onChange={() => this.props.selectDish(this.props.id)} />
            </InnerContainer>
        </Container>
    }

}

function mapStateToProps(state, ownProps) {
    return {
        checked: state.dishes.selectedDishes.indexOf(ownProps.id) >= 0
    };
}

export default connect(mapStateToProps, {selectDish})(FoodCard);