import React, { Component } from 'react';
import styled from "styled-components";
import {Text} from "react-native";

const Container = styled.View`
    padding-left: 20px;
    padding-right: 20px;
    background: rgba(247, 162, 0, 0.7);
    height: 30px;
    border-radius: 15px;
    overflow: hidden;
    
    display: flex;
    align-items: center;
    justify-content: center;
    
    margin-bottom: 10px;
`;

export default class FoodPrice extends Component {
    render() {
        return <Container>
            <Text>$ {this.props.price}</Text>
        </Container>
    }
}