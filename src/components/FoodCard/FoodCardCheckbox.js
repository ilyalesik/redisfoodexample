import React, { Component } from 'react';
import {Image, TouchableHighlight} from "react-native";
import styled from 'styled-components'

const checkIcon = require('./check-symbol.png');
const addIcon = require('./add.png');

const CheckedButton = styled.Image`
    width: 50%;
    height: 50%;
`;

const Container = styled.TouchableHighlight`
    border-radius: 20;
    overflow: hidden;
    width: 40px;
    height: 40px;
    
    align-items: center;
    justify-content: center;
    background-color: ${({checked}) => !checked ? "transparent": "#FFFFFF"};
    border: solid 2px #FFFFFF;
`;

export default class FoodCardCheckbox extends Component {

    render() {
        const {checked}= this.props;
        return <Container checked={checked} onPress={() => this.props.onChange()}>
            <CheckedButton source={checked ? checkIcon : addIcon} />
        </Container>
    }

}