import React, { Component } from 'react';
import {Text, StyleSheet, View, ScrollView} from "react-native";
import FoodCard from "../FoodCard/FoodCard";
import {connect} from "react-redux";
import {loadDishes} from "../../ducks/dishes";

class HomePage extends Component {

    componentDidMount() {
        this.props.loadDishes();
    }

    render() {
        return <ScrollView>
            {this.props.dishes.map((image, i) => <FoodCard key={i} {...image} />)}
        </ScrollView>
    }
}

function mapStateToProps(state) {
    return {
        dishes: state.dishes.dishes
    };
}

export default connect(mapStateToProps, {loadDishes})(HomePage);