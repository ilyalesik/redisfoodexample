
export const DISHES = [
    {
        id: 1,
        img: "https://upload.wikimedia.org/wikipedia/commons/thumb/6/6d/Good_Food_Display_-_NCI_Visuals_Online.jpg/1200px-Good_Food_Display_-_NCI_Visuals_Online.jpg",
        title: "Crockpot Lemon Garlic Chicken",
        price: 17.5
    },
    {
        id: 2,
        img: "https://s2.eda.ru/StaticContent/Photos/120214133254/120727184210/p_O.jpg",
        title: "Салат с ростбифом + айсберг, грейпфрут",
        price: 10
    },
    {
        id: 3,
        img: "https://takprosto.cc/wp-content/uploads/f/fruktovyi-salat/1.jpg",
        title: "Фруктовый салат"
    },
    {
        id: 4,
        img: "https://avatars.mds.yandex.net/get-pdb/224463/5e0de30e-ff8a-420b-9c91-880857ee022e/s800",
        title: "Бургер за говядиной",
        price: 15.5
    },
    {
        id: 5,
        img: "https://www.gastronom.ru/binfiles/images/20161226/bb647ec7.jpg",
        title: "Борщ"
    },
    {
        id: 6,
        img: "https://static.1000.menu/img/content/1336/salat-tsezar-s-kuritsei_1469194351_3_max.jpg",
        title: "Цезарь с курицей"
    }
];